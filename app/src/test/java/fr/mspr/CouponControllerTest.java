package fr.mspr;

import javax.servlet.http.HttpSession;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import fr.mspr.service.CouponService;
import fr.mspr.service.UserService;

@SpringBootTest
@WebAppConfiguration
class CouponControllerTest {
	
	@Autowired
	WebApplicationContext webApplicationContext;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private CouponService couponService;
	
	private MockMvc mvc;
	
	private HttpSession session2;
	
	protected MockHttpSession session;
	
	@BeforeEach 
	protected void setup() throws Exception {
		this.mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		this.session2 = this.mvc.perform(MockMvcRequestBuilders.get("/getuser")
			   .param("pseudo", "george")
			   .param("password", "Azerty01"))
              .andReturn().getRequest().getSession();
	}
	
	@Test
	void getCouponTest() throws Exception {
		 MvcResult result = this.mvc
		   			.perform(MockMvcRequestBuilders.get("/getcoupons"))
		   			.andReturn();
		   Assertions.assertEquals(302,result.getResponse().getStatus());
	}
	
	@Test
	void getCouponById() throws Exception {
		 MvcResult result = this.mvc
		   			.perform(MockMvcRequestBuilders.get("/getcoupon")
		   					.param("idCoupon", "2")
		   					.accept(MediaType.APPLICATION_JSON))
		   			.andReturn();
		   Assertions.assertEquals(302,result.getResponse().getStatus());
	}
	
	@Test
	void getCouponByIdKo() throws Exception {
		 MvcResult result = this.mvc
		   			.perform(MockMvcRequestBuilders.get("/getcoupon")
		   					.param("idCoupon", "")
		   					.accept(MediaType.APPLICATION_JSON))
		   			.andReturn();
		   Assertions.assertEquals(400,result.getResponse().getStatus());
	}

}
