package fr.mspr.factory;

import static org.junit.jupiter.api.Assertions.assertThrows;

import fr.mspr.model.entities.Coupon;
import fr.mspr.model.factory.PartenaireFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;

import fr.mspr.model.entities.Partenaire;
import fr.mspr.model.exception.CouponException;
import fr.mspr.model.exception.PartenaireException;
import fr.mspr.model.factory.CouponFactory;

import javax.xml.stream.FactoryConfigurationError;

@SpringBootTest
@WebAppConfiguration
class CouponFactoryTest {

	private final String nom = "nom";
	private final String motif = "motif";
	private final String code = "code";
	private final String date = "date";
	private final boolean valide = true;
	private final long id = 1;

	@Test
	void getUserFromParamWithIdTest() throws CouponException, PartenaireException {
		Partenaire p = PartenaireFactory.getPartenaireEmpty();
		assertThrows(PartenaireException.class, () -> CouponFactory.getCouponFromParam(this.id, this.nom, this.motif, this.code, this.date, this.valide, null));
		assertThrows(CouponException.class, () -> CouponFactory.getCouponFromParam(0, this.nom, this.motif, this.code, this.date, this.valide, p));
		assertThrows(CouponException.class, () -> CouponFactory.getCouponFromParam(this.id, "", this.motif, this.code, this.date, this.valide, p));
		assertThrows(CouponException.class, () -> CouponFactory.getCouponFromParam(this.id, this.nom, "", this.code, this.date, this.valide, p));
		assertThrows(CouponException.class, () -> CouponFactory.getCouponFromParam(this.id, this.nom, this.motif,"", this.date, this.valide, p));
		assertThrows(CouponException.class, () -> CouponFactory.getCouponFromParam(this.id, this.nom, this.motif, this.code, "", this.valide, p));
		Assertions.assertNotNull(CouponFactory.getCouponFromParam(this.id, this.nom, this.motif, this.code, this.date, this.valide, p));
	}

	@Test
	void getUserWithoutParam(){
		Coupon c = CouponFactory.getCouponEmpty();
		Assertions.assertNotNull(c);
	}
}
