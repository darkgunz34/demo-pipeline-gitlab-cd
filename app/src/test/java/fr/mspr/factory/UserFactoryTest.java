package fr.mspr.factory;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;

import fr.mspr.model.entities.User;
import fr.mspr.model.exception.UserException;
import fr.mspr.model.factory.UserFactory;

@SpringBootTest
@WebAppConfiguration
class UserFactoryTest {
	private final String pseudo = "george";
	private final String password = "Azerty01";
	private final String mail = "monMail@gmail.com";
	private final String nom = "garnier";
	private final String prenom = "george";
	private final long id = 1;

	@Test
	void getUserWithPseudoAndPasswordTest() throws UserException {
		User sut =  UserFactory.getUserWithPseudoAndPassword(this.pseudo, this.password);
		Assertions.assertEquals(this.pseudo,sut.getPseudo());
		Assertions.assertEquals(this.password,sut.getPassword());
		Assertions.assertThrows(UserException.class, () -> UserFactory.getUserWithPseudoAndPassword("", this.password));
		Assertions.assertThrows(UserException.class, () -> UserFactory.getUserWithPseudoAndPassword(this.pseudo, ""));
		User u = UserFactory.getUserWithPseudoAndPassword(pseudo, password);
		Assertions.assertNotNull(u);
	}

	@Test
	void getUserWithoutParam(){
		User u = UserFactory.getUserEmpty();
		Assertions.assertNotNull(u);
	}
	
	@Test
	void getUserFromParamWithIdTest() throws UserException {
		Assertions.assertThrows(UserException.class, () -> UserFactory.getUserFromParamWithId(0, this.pseudo, this.password, this.mail, this.prenom, this.nom));
		Assertions.assertThrows(UserException.class, () -> UserFactory.getUserFromParamWithId(this.id, "", this.password, this.mail, this.prenom, this.nom));
		Assertions.assertThrows(UserException.class, () -> UserFactory.getUserFromParamWithId(this.id, this.pseudo,"", this.mail, this.prenom, this.nom));
		Assertions.assertThrows(UserException.class, () -> UserFactory.getUserFromParamWithId(this.id, this.pseudo, this.password,"", this.prenom, this.nom));
		Assertions.assertThrows(UserException.class, () -> UserFactory.getUserFromParamWithId(this.id, this.pseudo, this.password, this.mail,"", this.nom));
		Assertions.assertThrows(UserException.class, () -> UserFactory.getUserFromParamWithId(this.id, this.pseudo, this.password, this.mail, this.prenom,""));
		User u = UserFactory.getUserFromParamWithId(1,"george","bob", "mail@bob.fr", this.prenom, this.nom);
		Assertions.assertNotNull(u);
	}
}
