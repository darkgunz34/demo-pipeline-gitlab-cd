package fr.mspr;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.jayway.jsonpath.JsonPath;

import fr.mspr.model.exception.UserException;
import fr.mspr.model.utils.UtilsEntity;

@SpringBootTest
@WebAppConfiguration
class UserControllerTest {
	
	private MockMvc mvc;
	
	protected MockHttpSession session = new MockHttpSession();
	
	@Autowired
	WebApplicationContext webApplicationContext;
	
	@BeforeEach 
	protected void setup() throws Exception {
		this.mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		this.session.setAttribute("key", "moncookieperso2");
	}

	
    @Test
    void seconnecter_identifiant_ko_test() throws UserException {
        final String name = "bob";
        final String pass = "pass";
        
        UtilsEntity.champNonVideUser(name);
        UtilsEntity.champNonVideUser(pass);
        Assertions.assertTrue(true);
    }

   @Test
    void seconnecter_test_ok() throws Exception {
    	MvcResult result = this.mvc
    			.perform(MockMvcRequestBuilders.get("/getuser")
    					.param("pseudo", "george")
    					.param("password", "Azerty01")
    					.accept(MediaType.APPLICATION_JSON))
    			.andExpect(MockMvcResultMatchers.status().is3xxRedirection())
    			.andReturn();
    	
    	int userId = JsonPath.parse(result.getResponse().getContentAsString()).read("$.id");
    	String userPseudo = JsonPath.parse(result.getResponse().getContentAsString()).read("$.pseudo");

    	Assertions.assertEquals(2, userId);
    	Assertions.assertEquals("george", userPseudo);
    }
   
   @Test
   void getUserCouponTest() throws Exception {
	   MvcResult result = this.mvc
	   			.perform(MockMvcRequestBuilders.get("/getusercoupons")
	   					.param("idUser", "2")
	   					.param("idCoupon", "1").session(this.session)
	   					.accept(MediaType.APPLICATION_JSON))
	   			.andReturn();
		   Assertions.assertEquals(302,result.getResponse().getStatus());
   }

	@Test
	void addCoupon_test_ok() throws Exception {
		MvcResult result = this.mvc
				.perform(MockMvcRequestBuilders.post("/addCoupon")
						.param("idUser", "2")
						.param("idCoupon", "1").session(this.session)
						.accept(MediaType.APPLICATION_JSON))
				.andReturn();
		Assertions.assertEquals(200,result.getResponse().getStatus());
	}

	@Test
	void addCoupon_test_ko() throws Exception {
		MvcResult result = this.mvc
				.perform(MockMvcRequestBuilders.post("/addCoupon")
						.param("idUser", "2")
						.param("idCoupon", "0").session(this.session)
						.accept(MediaType.APPLICATION_JSON))
				.andReturn();
		Assertions.assertEquals(400,result.getResponse().getStatus());
	}
   
   @Test
   void addCoupon_test_ko_401() throws Exception { 
	   MvcResult result = this.mvc
   			.perform(MockMvcRequestBuilders.post("/addCoupon")
   					.param("idUser", "2")
   					.param("idCoupon", "1")
   					.accept(MediaType.APPLICATION_JSON))
   			.andReturn();
	   Assertions.assertEquals(401,result.getResponse().getStatus());
   }
   
   @Test
   void delCoupon() throws Exception {
	   MvcResult result = this.mvc
	   			.perform(MockMvcRequestBuilders.delete("/delCoupon")
	   					.param("idUser", "2")
	   					.param("idCoupon", "1").session(this.session)
	   					.accept(MediaType.APPLICATION_JSON))
	   			.andReturn();
	   Assertions.assertEquals(200,result.getResponse().getStatus());
   }
   
   @Test
   void delCouponKo() throws Exception {
	   MvcResult result = this.mvc
	   			.perform(MockMvcRequestBuilders.delete("/delCoupon")
	   					.param("idUser", "2")
	   					.param("idCoupon", "1")
	   					.accept(MediaType.APPLICATION_JSON))
	   			.andReturn();
	   Assertions.assertEquals(401,result.getResponse().getStatus());
   }
   
   @Test
   void logout_test() throws Exception {
	   this.mvc.perform(MockMvcRequestBuilders.get("/logout")
	   					.session(this.session)
	   					.accept(MediaType.APPLICATION_JSON))
	   			.andReturn().getRequest().getSession();
	   Assertions.assertNull(this.session.getAttribute("key"));
   }
}
