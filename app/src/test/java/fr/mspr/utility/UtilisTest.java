package fr.mspr.utility;

import fr.mspr.model.entities.Coupon;
import fr.mspr.model.entities.User;
import fr.mspr.model.exception.CouponException;
import fr.mspr.model.exception.PartenaireException;
import fr.mspr.model.exception.UserException;
import fr.mspr.model.utils.UtilsEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class UtilisTest {

    private final String value = "Chaine non vide";

    @Test
    void champNonVidePartenaire(){
        try{
            UtilsEntity.champNonVidePartenaire(1);
        }catch (PartenaireException e){
            Assertions.fail();
        }
    }

    @Test
    void champNonVidePartenaireException(){
        try{
            UtilsEntity.champNonVidePartenaire(0);
            Assertions.fail();
        }catch (PartenaireException e){
        }
    }

    @Test
    void champNonVideCoupon(){
        try{
            UtilsEntity.champNonVideCoupon(1);
        }catch (CouponException e){
            Assertions.fail();
        }
    }

    @Test
    void champNonVideCouponException(){
        try{
            UtilsEntity.champNonVideCoupon(0);
            Assertions.fail();
        }catch (CouponException e){
        }
    }

    @Test
    void champNonVideUser(){
        try{
            UtilsEntity.champNonVideUser(1);
        }catch (UserException e){
            Assertions.fail();
        }
    }

    @Test
    void champNonVideUserException(){
        Assertions.assertThrows(UserException.class,() -> UtilsEntity.champNonVideUser(0));
    }

    @Test
    void champNonVidePartenaireString() throws PartenaireException {
        UtilsEntity.champNonVidePartenaire(this.value);
        Assertions.assertTrue(true);
    }

    @Test
    void champNonVidePartenaireExceptionString(){
        Assertions.assertThrows(PartenaireException.class,() -> UtilsEntity.champNonVidePartenaire(null));
    }

    @Test
    void champNonVideCouponString() throws CouponException {
        UtilsEntity.champNonVideCoupon(this.value);
        Assertions.assertTrue(true);
    }

    @Test
    void champNonVideCouponExceptionString(){
        Assertions.assertThrows(CouponException.class,() -> UtilsEntity.champNonVideCoupon((String) null));
    }

    @Test
    void champNonVideUserString() throws UserException {
        UtilsEntity.champNonVideUser(this.value);
        Assertions.assertTrue(true);
    }

    @Test
    void champNonVideUserExceptionString(){
        Assertions.assertThrows(UserException.class,() -> UtilsEntity.champNonVideUser(null));
    }

    @Test
    void userNullInuserOrCouponIsNull(){
        if(!UtilsEntity.userOrCouponIsNull(null,new Coupon())){
            Assertions.fail();
        }
    }

    @Test
    void couponNullInuserOrCouponIsNull(){
        if(!UtilsEntity.userOrCouponIsNull(new User(),null)){
            Assertions.fail();
        }
    }

    @Test
    void userOrCouponIsNull(){
        if(UtilsEntity.userOrCouponIsNull(new User(),new Coupon())){
            Assertions.fail();
        }
    }

    @Test
    void mailNonValide(){
        final String mailNonValide = "monmail@nonvalide";
        Assertions.assertThrows(UserException.class,() -> UtilsEntity.nonValideMailUser(mailNonValide));
    }
}
