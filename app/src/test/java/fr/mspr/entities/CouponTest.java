package fr.mspr.entities;

import fr.mspr.model.entities.Coupon;
import fr.mspr.model.entities.Partenaire;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class CouponTest {

    private long id;
    private String nom;
    private String motif;
    private String code;
    private String dateValidation;
    private boolean valide;
    private Partenaire partenaire;
    private Coupon coupon;

    @BeforeEach
    void setup(){
        this.coupon = new Coupon();
        this.partenaire = new Partenaire();
        this.id = 1;
        this.nom = "Mon nom";
        this.motif = "Mon motif";
        this.code = "Mon code";
        this.dateValidation = "12 Février 2021";
        this.valide = true;
        this.partenaire = new Partenaire();
    }

    @Test
    void init(){
        Assertions.assertNotNull(this.coupon);
    }

    @Test
    void getSetId(){
        this.coupon.setId(this.id);
        Assertions.assertEquals(this.coupon.getId(), this.id);
    }

    @Test
    void getSetCode(){
        this.coupon.setCode(this.code);
        Assertions.assertEquals(this.coupon.getCode(), this.code);
    }

    @Test
    void getSetMotif(){
        this.coupon.setMotif(this.motif);
        Assertions.assertEquals(this.coupon.getMotif(), this.motif);
    }

    @Test
    void getSetNom(){
        this.coupon.setNom(this.nom);
        Assertions.assertEquals(this.coupon.getNom(), this.nom);
    }

    @Test
    void getSetDateValidation(){
        this.coupon.setDateValidation(this.dateValidation);
        Assertions.assertEquals(this.coupon.getDateValidation(), this.dateValidation);
    }

    @Test
    void getSetPartenaire(){
        this.coupon.setPartenaire(this.partenaire);
        Assertions.assertEquals(this.coupon.getPartenaire(), this.partenaire);
    }

    @Test
    void getSetValide(){
        this.coupon.setValide(this.valide);
        Assertions.assertEquals(this.coupon.isValide(), this.valide);
    }

    @Test
    void getToString(){
        this.coupon = new Coupon(1, this.nom, this.motif, this.code, this.dateValidation, this.valide, this.partenaire);
        Assertions.assertEquals(this.coupon.toString(),"Coupon{id=1, nom='"+ this.nom +"', motif='"+ this.motif +"', code='"+ this.code +"', dateValidation='"+ this.dateValidation +"', valide="+ this.valide +", partenaire="+ this.partenaire +"}");
    }

    @Test
    void initWithParam(){
        this.coupon = new Coupon(1, this.nom, this.motif, this.code, this.dateValidation, this.valide, this.partenaire);
        Assertions.assertNotNull(this.coupon);
        Assertions.assertEquals(this.coupon.getNom(), this.nom);
        Assertions.assertEquals(this.coupon.getId(), this.id);
        Assertions.assertEquals(this.coupon.getCode(), this.code);
        Assertions.assertEquals(this.coupon.getDateValidation(), this.dateValidation);
        Assertions.assertEquals(this.coupon.isValide(), this.valide);
        Assertions.assertEquals(this.coupon.getPartenaire(), this.partenaire);
    }


}
