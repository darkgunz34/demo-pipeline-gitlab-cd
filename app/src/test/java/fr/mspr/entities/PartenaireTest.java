package fr.mspr.entities;

import fr.mspr.model.entities.Coupon;
import fr.mspr.model.entities.Partenaire;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class PartenaireTest {

    private long id;
    private String nom;
    private String adresse;
    private Partenaire partenaire;

    @BeforeEach
    void setup(){
        this.id = 1;
        this.nom = "Mon nom";
        this.adresse = "Mon motif";
        this.partenaire = new Partenaire();
    }

    @Test
    void init(){
        Assertions.assertNotNull(this.partenaire);
    }

    @Test
    void getSetId(){
        this.partenaire.setId(this.id);
        Assertions.assertEquals(this.id, this.partenaire.getId());
    }

    @Test
    void getSetAdresse(){
        this.partenaire.setAdresse(this.adresse);
        Assertions.assertEquals(this.adresse, this.partenaire.getAdresse());
    }

    @Test
    void getSetNom(){
        this.partenaire.setNom(this.nom);
        Assertions.assertEquals(this.nom, this.partenaire.getNom());
    }

    @Test
    void getToString(){
        this.partenaire = new Partenaire(this.id, this.nom, this.adresse);
        Assertions.assertEquals("Partenaire{id="+ this.id +", nom='"+ this.nom +"', adresse='"+ this.adresse +"'}", this.partenaire.toString());
    }

    @Test
    void initWithParam(){
        this.partenaire = new Partenaire(this.id, this.nom, this.adresse);
        Assertions.assertNotNull(this.partenaire);
        Assertions.assertEquals(this.partenaire.getNom(), this.nom);
        Assertions.assertEquals(this.partenaire.getId(), this.id);
        Assertions.assertEquals(this.partenaire.getAdresse(), this.adresse);
    }


}
