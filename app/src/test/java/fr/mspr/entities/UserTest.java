package fr.mspr.entities;

import fr.mspr.model.entities.Coupon;
import fr.mspr.model.entities.Partenaire;
import fr.mspr.model.entities.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class UserTest {

    private long id;
    private String pseudo;
    private String password;
    private String mail;
    private String nom;
    private String prenom;
    private List<Coupon> lstCoupon;
    private Coupon coupon1;
    private Coupon coupon2;
    private User user;

    @BeforeEach
    void setup(){
        this.user = new User();
        this.coupon1 = new Coupon();
        this.coupon2 = new Coupon();
        this.lstCoupon = new ArrayList<>();
        this.id = 1;
        this.nom = "Mon nom";
        this.pseudo = "pseudo";
        this.password = "password";
        this.mail = "monmail@gmail.com";
        this.nom = "mon nom";
        this.prenom = "mon prénom";
    }

    @Test
    void init(){
        Assertions.assertNotNull(this.user);
    }

    @Test
    void getSetId(){
        this.user.setId(this.id);
        Assertions.assertEquals(this.user.getId(), this.id);
    }

    @Test
    void getSetPseudo(){
        this.user.setPseudo(this.pseudo);
        Assertions.assertEquals(this.user.getPseudo(), this.pseudo);
    }

    @Test
    void getSetPassword(){
        this.user.setPassword(this.password);
        Assertions.assertEquals(this.user.getPassword(), this.password);
    }

    @Test
    void getSetMail() {
        this.user.setMail(this.mail);
        Assertions.assertEquals(this.user.getMail(), this.mail);
    }

    @Test
    void getSetNom(){
        this.user.setNom(this.nom);
        Assertions.assertEquals(this.user.getNom(), this.nom);
    }

    @Test
    void getSetPrenom(){
        this.user.setPrenom(this.prenom);
        Assertions.assertEquals(this.user.getPrenom(), this.prenom);
    }

    @Test
    void getSetListCoupon(){
        this.lstCoupon.add(this.coupon1);
        this.lstCoupon.add(this.coupon2);
        this.user.setListCoupon(this.lstCoupon);

        Assertions.assertEquals(2, this.lstCoupon.size());
        Assertions.assertEquals(this.user.getListCoupon().size(), this.lstCoupon.size());
        Assertions.assertEquals(this.user.getListCoupon().get(0), this.lstCoupon.get(0));
        Assertions.assertEquals(this.user.getListCoupon().get(1), this.lstCoupon.get(1));
    }

    @Test
    void initWithFullParam(){
        this.user = new User(this.id, this.pseudo, this.password, this.mail, this.nom, this.prenom);
        Assertions.assertNotNull(this.user);
        Assertions.assertEquals(this.id, this.user.getId());
        Assertions.assertEquals(this.pseudo, this.user.getPseudo());
        Assertions.assertEquals(this.password, this.user.getPassword());
        Assertions.assertEquals(this.mail, this.user.getMail());
        Assertions.assertEquals(this.prenom, this.user.getPrenom());
        Assertions.assertEquals(0, this.user.getListCoupon().size());
    }

    @Test
    void initWithShortParam(){
        this.user = new User(this.pseudo, this.password);
        Assertions.assertNotNull(this.user);
        Assertions.assertEquals(this.pseudo, this.user.getPseudo());
        Assertions.assertEquals(this.password, this.user.getPassword());
    }

    @Test
    void addCoupon(){
        this.user.ajouterCoupon(this.coupon1);
        this.user.ajouterCoupon(this.coupon2);

        Assertions.assertEquals(2, this.user.getListCoupon().size());
    }

    @Test
    void suppressionCoupon(){
        this.lstCoupon.add(this.coupon1);
        this.lstCoupon.add(this.coupon2);
        this.user.setListCoupon(this.lstCoupon);
        Assertions.assertEquals(this.lstCoupon.size(), this.user.getListCoupon().size());
        this.user.removeCoupon(this.coupon1);
        Assertions.assertEquals(1, this.user.getListCoupon().size());
        Assertions.assertEquals(this.coupon2, this.user.getListCoupon().get(0));
    }

    @Test
    void getToString(){
        this.user = new User(this.id, this.pseudo, this.password, this.mail, this.nom, this.prenom);
        Assertions.assertEquals("User{id="+ this.id +", pseudo='"+ this.pseudo +"', mail='"+ this.mail +"', nom='"+ this.nom +"', prenom='"+ this.prenom +"', list_coupon=[]}", this.user.toString());
    }


}
