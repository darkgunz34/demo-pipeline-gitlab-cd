package fr.mspr;

import fr.mspr.model.exception.CustomGetMessage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class CustonGetMessageTest {

    @Test
    void testException(){
        try{
            throw new Exception("Custom Message à lire");
        }catch (Exception e){
            final String messagePrevu = "en tête\nfr.mspr.CustonGetMessageTest.testException(CustonGetMessageTest.java:14)\n";
            String message = CustomGetMessage.recuperationDerniereLigneException("en tête",e.getStackTrace(),1);
            e.printStackTrace();
            Assertions.assertEquals(messagePrevu,message);
        }
    }
}
