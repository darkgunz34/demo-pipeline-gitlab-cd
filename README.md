**PROJET :**

Le présent projet est issue de la premiere mise en situation professionnel reconstituer qui se concentre sur le développement d'une application mobile. Le cahier des charges fut fournis et les taches réalisées sont les suivantes :
- études des besoins fonctionnels
- Développement de l’application mobile
- Réalisation de WebServices adaptés
- Recette fonctionnelle
- Industrialisation des tests

**Composition du projet**

Le projet contient une API REST  permettant de récuper des données sous forme json d'élément pour un traitement de scann de QrCode.

Les qrCodes sont stockées en base de données et sont consommer à travers différents points d'entrée (en mode visiteur ou en mode connecter) ou encore la récupération des éléments contenu en base de donnée à des fins d'affichage. 

La partie de tests unitaires permet de tester l'ensemble de l'api afin d'assurer son bon fonctionnement.

**OBJECTIF :**

Ce projet à été déposer sur gitlab afin de pouvoir mettre en place un pipeline CI dans le cadre d'un travaux pratique devops.
L'objectif est de pouvoir déclencher un certain nombre de tâches, appelé job, afin de garantir une bonne integration du projet.

**MEMBRES :**
- Stephan Parichon
- Georges Garnier

**PIPELINE :**

Branche master :
Le pipeline présent sur la branche master permet de déclencher les étapes suivantes :
- build
- test
- install
- dockerization
- deploy

Branche sonarqube:
Le pipeline présent sur cette branche permet de déclencher les etapes suivantes :
- build
- test
- sonarQube
- install

Pour le scan de sonarqube, plusieurs configurations on été testées sans succès.

Les étapes de son implémentation sont les suivantes :

- Le module sonar scanner fut installé au sein du projet (npm install --save sonar-scanner)
- Puis, la version 9 de sonarqubeServer a été telecharger et lancer en local. 
- Une fois le serveur up, nous nous sommes connecté sur l'interface d'administration en suivant le lien suivant: http:/localhost:9000
- Une fois connecter avec les codes par défaut nous avons généré un token de connexion.
- Enfin, nous avons ajoutè les commandes nécessaires au sein du fichier gitlabci.yml avec les informations de connexion pour sonarqubeServer

Malheureusement le job échoue car gitlab n'est pas capable de joindre notre poste local. Nous avons également essayé d'utiliser le serveur de sonarqube par l'intermediaire d'une image docker mais le résultat reste le même. (connection refused: localhost:9000).
Après recherche il semblerait que ce soit une restriction mise en place par gitlab lui même. 

La seule solution que nous voyons serait d'investir dans un serveur sur lequel serait déployer sonarqube afin que gitlab puisse l'atteindre durant le job concerné.

**INSTALLATION :**

Une fois rattacher au projet, faites un clone de la branche qui vous interesse sur votre poste local. Il vous suffit ensuite de faire un commit + push afin que le pipeline démarre. La durée du traitement varie de 3 à 15 mins.


